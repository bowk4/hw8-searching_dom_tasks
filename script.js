// Exercise #1
const paragraphs = document.getElementsByTagName('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = '#ff0000';
}

console.log("Exercise #2")
const idName = document.getElementById('optionsList')
console.log(idName)
const parentIdName = idName.parentElement;
console.log(parentIdName)
if (idName.hasChildNodes()) {
    const childNodes = idName.childNodes;
    for (const node of childNodes) {
        if (node.nodeType === 1) {
            console.log("Name:", node.nodeName, "Type:", node.nodeType);
        }
    }
}

// Exercise #3
const testParagraph = document.querySelector('#testParagraph')
testParagraph.textContent = 'This is a paragraph'

console.log("Exercise #4")
///

console.log("Exercise #5")
const mainHeader = document.querySelector('.main-header').children
for (const mainHeaderElement of mainHeader) {
    // console.log(mainHeaderElement)
    mainHeaderElement.classList.toggle("nav-item")
    console.log(mainHeaderElement)
}

// Exercise #6
const sectionTitle = document.querySelectorAll('.section-title')
for (const element of sectionTitle) {
    element.classList.remove("section-title")
}
